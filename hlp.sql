-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 08:50 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hlp2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fName` varchar(30) NOT NULL,
  `admin_lName` varchar(30) NOT NULL,
  `admin_User` varchar(30) NOT NULL,
  `admin_pass` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fName`, `admin_lName`, `admin_User`, `admin_pass`) VALUES
(1, 'aian', 'lesigues', 'jade.aian', '123'),
(2, 'mario', 'espinosa', 'mar', '321');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `fromUserId`, `toUserId`, `comment`, `date`) VALUES
(106, 0, 0, 'lol', '2017-12-10 02:13:56'),
(107, 0, 0, '12312412', '2017-12-15 11:13:36'),
(108, 0, 0, 'mmmmmmmmm', '2017-12-15 11:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `userFname` varchar(50) NOT NULL,
  `userMname` varchar(50) NOT NULL,
  `userLname` varchar(50) NOT NULL,
  `userEmail` varchar(50) NOT NULL,
  `userPhone` varchar(50) NOT NULL,
  `userUsername` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `userFname`, `userMname`, `userLname`, `userEmail`, `userPhone`, `userUsername`, `user_password`) VALUES
(1, 'Keith Ivan', 'Chio', 'Bongbong', 'keithivanbongbong@gmail.com', '09199914669', 'keith', '123'),
(2, 'Mario', 'Fuentes', 'Espinosa III', 'mariofuentesespinosa@gmail.com', '09199914669', 'mario', '123');

-- --------------------------------------------------------

--
-- Table structure for table `user_post`
--

CREATE TABLE `user_post` (
  `postID` int(11) NOT NULL,
  `postTitle` varchar(255) NOT NULL,
  `postDesc` varchar(255) NOT NULL,
  `postCategory` varchar(255) NOT NULL,
  `postLocation` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `userID` int(11) NOT NULL,
  `postDateTime` varchar(255) NOT NULL,
  `firstName` varchar(11) NOT NULL,
  `lastName` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_post`
--

INSERT INTO `user_post` (`postID`, `postTitle`, `postDesc`, `postCategory`, `postLocation`, `status`, `userID`, `postDateTime`, `firstName`, `lastName`) VALUES
(26, 'Making a Swimming Pool', 'Kailangan mi maghimo ug pool karon adlawa ra kay karon rang adlawa nga gusto mi mangaligo.', 'Panday', 'Consolacion, Cebu, City', '1', 1, '09/12/17', '', ''),
(27, 'Magpalaba Kog Sanina', 'Kadyot ra jud kaau ni. Sakit kaayu akong kamot mao magpalaba ko pls palihug lng', 'Labandera or Labandero', 'Mandaue, City', '1', 1, '09/12/17', '', ''),
(28, 'Need a Veterinarian', 'Kay ganahan man ko', 'Veterinarian', 'Balay ni Via', '1', 1, '09/12/17', '', ''),
(29, 'University of San Carlos needs a Janitor', 'Kanang makalimpyo ug tarung nya kanang maka night shift kay aron limpyo ang skwelahan 24/7', 'Janitor', 'Talamban, Cebu', '1', 1, '10/12/17', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `workerinfo_application`
--

CREATE TABLE `workerinfo_application` (
  `worker_application` int(11) NOT NULL,
  `nbi_clearance` varchar(255) NOT NULL,
  `skill_certificate` varchar(255) NOT NULL,
  `B_P_clearance` varchar(255) NOT NULL,
  `worker_fName` varchar(30) NOT NULL,
  `worker_mName` varchar(30) NOT NULL,
  `worker_lName` varchar(30) NOT NULL,
  `worker_address` varchar(50) NOT NULL,
  `worker_city` varchar(20) NOT NULL,
  `wZip` varchar(10) NOT NULL,
  `wProvince` varchar(50) NOT NULL,
  `worker_age` varchar(20) NOT NULL,
  `worker_birthday` varchar(30) NOT NULL,
  `worker_bPlace` varchar(50) NOT NULL,
  `worker_mobileNum` varchar(11) NOT NULL,
  `worker_email` varchar(50) NOT NULL,
  `worker_gender` varchar(10) NOT NULL,
  `worker_cStatus` varchar(20) NOT NULL,
  `worker_TIN` varchar(30) NOT NULL,
  `application_Status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workerinfo_application`
--

INSERT INTO `workerinfo_application` (`worker_application`, `nbi_clearance`, `skill_certificate`, `B_P_clearance`, `worker_fName`, `worker_mName`, `worker_lName`, `worker_address`, `worker_city`, `wZip`, `wProvince`, `worker_age`, `worker_birthday`, `worker_bPlace`, `worker_mobileNum`, `worker_email`, `worker_gender`, `worker_cStatus`, `worker_TIN`, `application_Status`) VALUES
(1, '395.jpg', '29.jpg', '126.png', 'jude', 'Cabillan', 'pastor', 'slt', 'cebu', '6000', 'cebu', '19', '03-01-2017', 'cebu', '9210413118', 'jaydesierto@gmail.com', 'male', 'Single', 'None', 0),
(2, '471.jpg', '909.png', '13.png', 'jude', 'Cabillan', 'pastor', 'slt', 'cebu', '6000', 'cebu', '19', '17-01-2001', 'cebu', '9210413118', 'jaydesierto@gmail.com', 'male', 'Separated', 'aadawd123123ad', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `user_post`
--
ALTER TABLE `user_post`
  ADD PRIMARY KEY (`postID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `workerinfo_application`
--
ALTER TABLE `workerinfo_application`
  ADD PRIMARY KEY (`worker_application`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_post`
--
ALTER TABLE `user_post`
  MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `workerinfo_application`
--
ALTER TABLE `workerinfo_application`
  MODIFY `worker_application` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_post`
--
ALTER TABLE `user_post`
  ADD CONSTRAINT `user_post_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
