<?php
require_once '../controller/adminController.php';

$conn = new adminController();

//Set Default Date and Time: PHILIPPINES

date_default_timezone_set("Asia/Manila");

//Admin Login
if(isset($_POST['login'])){

	if (!$_POST['username'] || !$_POST['password']) {
		echo 'no data';
		return ;
	}
	else{
	$conn->login($_POST['username'],$_POST['password']);
	}
}

//User Update

if(isset($_POST['update'])){

  $data = ['email' => $_POST['email'],
           'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'address' => $_POST['address'],
            'mobilenumber' => $_POST['mobilenumber'],
						'updated_at' => date("Y-m-d h:i:sa")];

            var_dump($data);

	$conn->update_user($_POST['userid'],$data);

}

//Delete User

if(isset($_POST['delete'])){
  $conn->delete_user($_POST['userid']);
}

//Register Worker

if(isset($_POST['register_worker'])){

  foreach ($_POST as $key => $value){
  $post [$key] = $value;
  }

  unset($post['register_worker']);

	$post['password'] = md5($post['password']);

  $files = [
           'nbi_clearance' => basename($_FILES["nbiclearance"]["name"]),
           'skill_certificate' => basename($_FILES["skillcertificate"]["name"]),
           'b_p_clearance' => basename($_FILES["bpclearance"]["name"])
          ];

  $target_dir = "../assets/images/";

  foreach ($_FILES as $key2 => $value2){

    $target_file = $target_dir . $value2['name'];
    move_uploaded_file($_FILES[$key2]["tmp_name"], $target_file);
  }

  $data = array_merge($post,$files);

  $conn->form_validation_worker($post,$files);

  $conn->register_worker($data);
}

//Worker Update
if(isset($_POST['update_worker'])){

	foreach ($_POST as $key => $value){
		$post [$key] = $value;
	}

  $files = [
           'nbi_clearance' => basename($_FILES["nbiclearance"]["name"]),
           'skill_certificate' => basename($_FILES["skillcertificate"]["name"]),
           'b_p_clearance' => basename($_FILES["bpclearance"]["name"])];

  $target_dir = "../assets/images/";

  foreach ($_FILES as $key2 => $value2){
	  $target_file = $target_dir . $value2['name'];
	  move_uploaded_file($_FILES[$key2]["tmp_name"], $target_file);
  }

  $conn->form_validation_worker($post,$files);
}

?>
