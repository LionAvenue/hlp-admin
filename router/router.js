function login(){
  var username = $('#username').val();
  var password = $('#password').val();

  if(!username || !password){
    swal("Error!", "Field is Empty", "warning");
    return ;
  }

    $.ajax({
           url: "http://localhost/hlp-admin/router/router.php",
           type: "post",
           data: {username:username, password:password, login:'login'},
           success: function (data) {
              if(data == 'true'){
                swal({
                  title: "SUCCESS!",
                  text: "Welcome Back",
                  type: "success",
                  confirmButtonClass: 'btn-success',
                  confirmButtonText: 'Go to Dashboard!',
                  closeOnConfirm: false,
                },
                function(){
                    window.location = "http://localhost/hlp-admin/views/dashboard.php";
                });

              }else{
                swal("Warning!", "User Doesn't Exist!", "warning");
              }
           },
           error: function(error) {
              swal("Error!", "Error Router!", "error");
              console.log(error);
           }
       });
}
