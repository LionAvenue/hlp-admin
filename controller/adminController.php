<?php
/**
*
*/
class adminController {
public $conn;


public function __construct() {
	$this->connect_db();
}

private function connect_db(){

	// Create connection
	$this->conn = new mysqli('localhost', 'root', '', 'hlp');

	// Check connection
	if(!$this->conn){
			$this->error = "Fatal Error: Can't connect to database";
			return false;
		}

}

public function login($username,$password){
	$username = strtolower($username);
	$password = md5($password);

	$stmt = $this->conn->prepare("SELECT * FROM `admin` WHERE `username` = '$username'  AND `password` = '$password'");

	if($stmt->execute()){
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$fetch = $result->fetch_array();
			session_start();
			$_SESSION['admin'] = $fetch;
      echo 'true';
      }else{
        echo 'false';
       }
	}
}

public function get_user(){
  $result = mysqli_query($this->conn,"SELECT * FROM `customers` WHERE availability = 1");
    if(!$result){
      return "NO DATA";
    }
    return $result;
}

public function update_user($userid,$data){
  $columns = join('=?,', array_keys($data)).'=?';
  $values = implode(", ", array_values($data));

  $sql = "UPDATE `customers` SET $columns WHERE user_id=?";

  $stmt = $this->conn->prepare($sql);

  $stmt->bind_param('sssssss',
                $data['email'],
              $data['firstname'],
            $data['lastname'],
          $data['address'],
        $data['mobilenumber'],
				$data['updated_at'],
      $userid);

  $stmt->execute();

  if ($stmt->errno) {
    echo "FAILURE!!! " . $stmt->error;
  }
  else echo "Updated rows";

  $stmt->close();

}

public function delete_user($userid){
  $availability = 0;

  $sql = "UPDATE `customers` SET availability=? WHERE user_id=?";

  $stmt = $this->conn->prepare($sql);

  $stmt->bind_param('ss', $availability,$userid);

  $stmt->execute();

  if ($stmt->errno) {
    echo "FAILURE!!! " . $stmt->error;
  }
  else echo "Updated rows";

  $stmt->close();
}

public function register_worker($data){

  $firstname = $data['firstname'];
  $lastname = $data['lastname'];

  $stmt = $this->conn->prepare("SELECT * FROM `workers` WHERE `firstname` = '$firstname'  AND `lastname` = '$lastname'");

	if($stmt->execute()){
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
      echo "worker already exist";
			}else{
      $columns = join(', ', array_keys($data));
      $insert = "'" . implode ( "', '", array_values($data) ) . "'";

      mysqli_query($this->conn,"INSERT INTO `workers` ($columns) VALUES ($insert)");
			header('Location: http://localhost/hlp-admin/views/add_worker.php');
	    }
  }
}

public function get_worker(){
  $result = mysqli_query($this->conn,"SELECT * FROM `workers`");
    if(!$result){
      return "NO DATA";
    }
    return $result;
}

public function get_worker_byid($userid){
  $stmt = $this->conn->prepare("SELECT * FROM `workers` WHERE worker_id = '$userid'");

  if($stmt->execute()){
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
      $fetch = $result->fetch_array();

       return $fetch;

      }else{
       echo "no data found";}
  }
}

public function update_worker($data){
	$worker_id = $data['worker_id'];

	unset($data['worker_id']);

	$columns = implode(', ', array_map(
    function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
    $data,
    array_keys($data)
));


mysqli_query($this->conn,"UPDATE `workers` SET $columns WHERE worker_id='$worker_id'");

$url = "http://localhost/hlp-admin/views/edit_worker.php?editworker='$worker_id'";

header('Location: ' . $_SERVER['HTTP_REFERER']);

}

public function delete_worker(){

}

public function form_validation_worker($post,$files){

foreach ($post as $key => $value) {
  $value = trim($value);
    if (empty($value)){
        return "field empty";
      }
    }

$check = 0;
foreach ($files as $key2 => $value2) {
  $value2 = trim($value2);
    if (empty($value2)){
				$check = $check + 1;
        unset($files[$key2]);
      }
    }

	if($check == 3){
		$files = [];
	}

  $post = array_map('strtolower', $post);

  $data = array_merge($post,$files);

	if ($data['update_worker']) {
		unset($data['update_worker']);
		$this->update_worker($data);
		return ;
	}

  $this->register_worker($data);
}
}

?>
