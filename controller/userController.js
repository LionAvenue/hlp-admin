function user_update(id){
    $('#user_updt_btn_'+id).hide();
    $('#user_dlt_btn_'+id).hide();
    $('#user_sv_btn_'+id).show();
    $('#user_cncl_btn_'+id).show();

    $('#input_email_'+id).show();
    $('#input_firstname_'+id).show();
    $('#input_lastname_'+id).show();
    $('#input_address_'+id).show();
    $('#input_mobilenumber_'+id).show();

    $('#output_email_'+id).hide();
    $('#output_firstname_'+id).hide();
    $('#output_lastname_'+id).hide();
    $('#output_address_'+id).hide();
    $('#output_mobilenumber_'+id).hide();

}

function user_delete(id){
  swal({
    title: "Suspend?",
    text: "Are you sure you want to Suspend this user!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: 'btn-danger',
    confirmButtonText: 'Yes, Please!',
    closeOnConfirm: false,
    //closeOnCancel: false
  },
  function(){
    $.ajax({
           url: "http://localhost/hlp-admin/router/router.php",
           type: "post",
           data: {userid:id, delete:'delete'},
           success: function (data) {
              swal({
                title: "Deleted?",
                text: "Your imaginary file has been deleted!",
                type: "success",
                confirmButtonClass: 'btn-success',
                confirmButtonText: 'Nice!',
              },  function(){
                   window.location = "http://localhost/hlp-admin/views/user_profile.php";
                  });
           },
           error: function(error) {
              console.log(error);
           }
       });
  });
}

function user_cancel(id){
    $('#user_updt_btn_'+id).show();
    $('#user_dlt_btn_'+id).show();
    $('#user_sv_btn_'+id).hide();
    $('#user_cncl_btn_'+id).hide();

    $('#input_email_'+id).hide();
    $('#input_firstname_'+id).hide();
    $('#input_lastname_'+id).hide();
    $('#input_address_'+id).hide();
    $('#input_mobilenumber_'+id).hide();

    $('#output_email_'+id).show();
    $('#output_firstname_'+id).show();
    $('#output_lastname_'+id).show();
    $('#output_address_'+id).show();
    $('#output_mobilenumber_'+id).show();

      swal("Cancelled", "Your imaginary file is safe :)", "error");
}

function user_save(id){
  var firstname = $('#input_firstname_'+id).val();
  var lastname = $('#input_lastname_'+id).val();
  var address = $('#input_address_'+id).val();
  var mobilenumber = $('#input_mobilenumber_'+id).val();
  var email = $('#input_email_'+id).val();
  var thisDate = $('#input_date').val();
  var regex = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g;


  if(!firstname || !lastname || !address || !mobilenumber || !email){
    swal("Cancelled", "Empty Field", "error");
    return
  }

   $.ajax({
          url: "http://localhost/hlp-admin/router/router.php",
          type: "post",
          data: {userid:id,email:email,firstname:firstname,lastname:lastname,address:address,mobilenumber:mobilenumber,update:'update'},
          success: function (data) {
            $('#output_email_'+id).text(email);
            $('#output_firstname_'+id).text(firstname);
            $('#output_lastname_'+id).text(lastname);
            $('#output_address_'+id).text(address);
            $('#output_mobilenumber_'+id).text(mobilenumber);
            $('#output_updatedat_'+id).text(thisDate);

            user_cancel(id);
            $('#output_mobilenumber_'+id).show();
            swal("Good job!", "User Updated!", "success");
          },
          error: function(error) {
             console.log(error);
          }
      });
}
