<?php  include('layouts/header.php'); ?>

    <div class="wrapper">
      <?php  include('layouts/side-navigation.php'); ?>
        <div class="main-panel">
            <!-- Navbar -->
            <?php include('layouts/header-navigation.php'); ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Worker</h4>
                                    <p class="card-category">Add Worker</p>
                                </div>
                                <div class="card-body ">
                                    <form class="" action="../router/router.php" method="post" enctype="multipart/form-data">
                                      <div class="form-group col-md-12">
                                        <label for="inputCity">Position</label>
                                        <input type="text" name="position" class="form-control" id="inputCity" placeholder="Position">
                                      </div>
                                      <div class="form-row">
                                      <div class="form-group col-md-5">
                                        <label for="inputCity">Firstname</label>
                                        <input type="text" name="firstname" class="form-control" id="inputCity" placeholder="Firstname">
                                      </div>
                                      <div class="form-group col-md-5">
                                        <label for="inputState">Lastname</label>
                                        <input type="text" name="lastname" class="form-control" id="inputZip" placeholder="Lastname">
                                      </div>
                                      <div class="form-group col-md-2">
                                        <label for="inputZip">Middle Name</label>
                                        <input type="text" name="middlename" class="form-control" id="inputZip" placeholder="Middlename">
                                      </div>
                                      </div>
                                      <div class="form-row">
                                      <div class="form-group col-md-2">
                                        <label for="inputCity">Age</label>
                                        <input type="text" name="age" class="form-control" id="inputCity" placeholder="Age">
                                      </div>
                                      <div class="form-group col-md-5">
                                        <label for="inputState">Birthday</label>
                                        <input type="date" name="birthday" class="form-control" id="inputZip" placeholder="Birthday">
                                      </div>
                                      <div class="form-group col-md-5">
                                        <label for="inputZip">Birthday Place</label>
                                        <input type="text" name="birth_place" class="form-control" id="inputZip" placeholder="Birth Place">
                                      </div>
                                      </div>
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                          <label for="inputEmail4">Email</label>
                                          <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label for="inputPassword4">Password</label>
                                          <input type="password" name="password" class="form-control" id="inputPassword4" placeholder="Password">
                                        </div>
                                        </div>
                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                          <label for="inputCity">Gender</label>
                                          <select id="inputState" name="gender" class="form-control">
                                           <option></option>
                                           <option value="male">Male</option>
                                           <option value="female">Female</option>
                                         </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                          <label for="inputState">Mobile No.</label>
                                          <input type="text" name="mobilenumber" class="form-control" id="inputCity" placeholder="Mobile No.">
                                        </div>
                                        <div class="form-group col-md-4">
                                          <label for="inputZip">Civil Status</label>
                                          <input type="text" name="civil_status" class="form-control" id="inputZip" placeholder="Civil Status">
                                        </div>
                                        </div>
                                        <div class="form-group">
                                        <label for="inputAddress">Address</label>
                                        <input type="text" class="form-control" name="address" id="inputAddress" placeholder="1234 Main St">
                                        </div>
                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                          <label for="inputCity">City</label>
                                          <input type="text" name="city" class="form-control" id="inputCity">
                                        </div>
                                        <div class="form-group col-md-4">
                                          <label for="inputState">Province</label>
                                          <input type="text" name="province" class="form-control" id="inputCity">
                                        </div>
                                        <div class="form-group col-md-4">
                                          <label for="inputZip">Zip</label>
                                          <input type="text" name="zip" class="form-control" id="inputZip">
                                        </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                          <div class="form-group col-md-4">
                                            <label for="inputZip">Tin</label>
                                            <input type="text" name="tin" class="form-control" id="inputZip">
                                          </div>
                                          <label for="inputZip">Nbi Clearance</label>
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="nbiclearance" id="validatedCustomFile" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label for="inputZip">Skill Certificate</label>
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="skillcertificate" id="validatedCustomFile" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <label for="inputZip">BP Clearance</label>
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="bpclearance" id="validatedCustomFile" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                          </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="register_worker" value="register_worker">Sign in</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <?php  include('layouts/footer.php'); ?>
