<?php  include('layouts/header.php'); ?>

    <div class="wrapper">
      <?php  include('layouts/side-navigation.php'); ?>
        <div class="main-panel">
            <!-- Navbar -->
            <?php include('layouts/header-navigation.php'); ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Title</h4>
                                    <p class="card-category">Sub Header</p>
                                </div>
                                <div class="card-body ">
                                    blank page
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <?php  include('layouts/footer.php'); ?>
