<?php  include('layouts/header.php');
require_once '../controller/adminController.php';
$conn = new adminController();

$data = $conn->get_user();
//Set Default Date and Time: PHILIPPINES
date_default_timezone_set("Asia/Manila");

?>

<div class="wrapper">
  <?php  include('layouts/side-navigation.php'); ?>
    <div class="main-panel">
        <!-- Navbar -->
        <?php include('layouts/header-navigation.php'); ?>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Light Bootstrap Table Heading</h4>
                                <p class="card-category">Created using Montserrat Font Family</p>
                            </div>
                            <div class="card-body">
                              <!-- Example DataTables Card-->
                              <div class="card mb-3">
                                <div class="card-body">
                                  <input type="text" id="input_date" name="input_date" value="<?php echo date("Y-m-d h:i:s");?>" style="display:none;">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                      <thead>
                                        <tr>
                                          <th>Email</th>
                                          <th>Firstname</th>
                                          <th>Lastname</th>
                                          <th>Address</th>
                                          <th>Mobile No.</th>
                                          <th>Created At</th>
                                          <th>Update At</th>
                                          <th>Options</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php  while($row = mysqli_fetch_array($data)){ ?>
                                        <tr>
                                          <td>
                                            <input type='text' id="input_email_<?php echo $row['user_id'] ?>" class='form-control' value="<?php echo $row['email'] ?>" style="display:none; width: 50%">
                                            <p id="output_email_<?php echo $row['user_id'] ?>"><?php echo $row['email'] ?></p></td>
                                          <td>
                                            <input type='text' id="input_firstname_<?php echo $row['user_id'] ?>" class='form-control' value="<?php echo $row['firstname'] ?>" style="display:none;  width: 50%">
                                            <p id="output_firstname_<?php echo $row['user_id'] ?>"><?php echo $row['firstname'] ?></p></td>
                                          <td>
                                            <input type='text' id="input_lastname_<?php echo $row['user_id'] ?>" class='form-control' value="<?php echo $row['lastname'] ?>" style="display:none;  width: 50%">
                                            <p id="output_lastname_<?php echo $row['user_id'] ?>"><?php echo $row['lastname'] ?></p></td>
                                          <td>
                                            <input type='text' id="input_address_<?php echo $row['user_id'] ?>" class='form-control' value="<?php echo $row['address'] ?>" style="display:none;  width: 50%">
                                            <p id="output_address_<?php echo $row['user_id'] ?>"><?php echo $row['address'] ?></p></td>
                                          <td>
                                            <input type='text' id="input_mobilenumber_<?php echo $row['user_id'] ?>" class='form-control' value="<?php echo $row['mobilenumber'] ?>" style="display:none;  width: 50%">
                                            <p id="output_mobilenumber_<?php echo $row['user_id'] ?>"><?php echo $row['mobilenumber'] ?></p></td>
                                            <td><?php echo $row['created_at'] ?></td>
                                            <td><p id="output_updatedat_<?php echo $row['user_id'] ?>"><?php echo $row['updated_at'] ?></p></td>
                                          <td>
                                            <button class="btn btn-primary" id="user_updt_btn_<?php echo $row['user_id'] ?>" onclick="user_update(<?php echo $row['user_id'] ?>)">Update</button>
                                            <button class="btn btn-danger" id="user_dlt_btn_<?php echo $row['user_id'] ?>" onclick="user_delete(<?php echo $row['user_id'] ?>)">Suspend</button>
                                            <button class="btn btn-success" id="user_sv_btn_<?php echo $row['user_id'] ?>" onclick="user_save(<?php echo $row['user_id'] ?>)" style="display:none">Save</button>
                                            <button class="btn btn-warning" id="user_cncl_btn_<?php echo $row['user_id'] ?>" onclick="user_cancel(<?php echo $row['user_id'] ?>)" style="display:none">Cancel</button>
                                          </td>
                                        </tr>
                                      <?php } ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  include('layouts/footer.php'); ?>
