<?php  include('layouts/header.php');
  require_once '../controller/adminController.php';
	$conn = new adminController();

  $data = $conn->get_worker();

 ?>

    <div class="wrapper">
      <?php  include('layouts/side-navigation.php'); ?>
        <div class="main-panel">
            <!-- Navbar -->
            <?php include('layouts/header-navigation.php'); ?>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Light Bootstrap Table Heading</h4>
                                    <p class="card-category">Created using Montserrat Font Family</p>
                                     <a href="http://localhost/hlp-admin/views/add_worker.php" class="btn btn-primary"><i class="nc-icon nc-simple-add"></i> Add Worker</a>
                                </div>
                                <div class="card-body">
                                  <!-- Example DataTables Card-->
                                  <div class="card mb-3">
                                    <div class="card-body">
                                      <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                          <thead>
                                            <tr>
                                              <th>Worker id</th>
                                              <th>Position</th>
                                              <th>Firstname</th>
                                              <th>Lastname</th>
                                              <th>Email</th>
                                              <th>Address</th>
                                              <th>Mobile No.</th>
                                              <th>Status</th>
                                              <th>Option</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php  while($row = mysqli_fetch_array($data)){ ?>
                                            <tr class="success">
                                              <td><?php echo $row['worker_id'] ?></td>
                                              <td><?php echo $row['position'] ?></td>
                                              <td><?php echo $row['firstname'] ?></td>
                                              <td><?php echo $row['lastname'] ?></td>
                                              <td><?php echo $row['email'] ?></td>
                                              <td><?php echo $row['address'] ?></td>
                                              <td><?php echo $row['mobilenumber'] ?></td>
                                              <td><?php echo $row['application_status'] ?></td>
                                              <td><?php echo "<a class='btn btn-primary' href=edit_worker.php?editworker=",$row['worker_id'],">Update</a>" ?></td>
                                            </tr>
                                          <?php } ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <?php  include('layouts/footer.php'); ?>
