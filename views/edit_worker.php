<?php  include('layouts/header.php');
  require_once '../controller/adminController.php';
	$conn = new adminController();

  $data = $conn->get_worker_byid($_GET['editworker']);

  switch ($data['application_status']) {
      case "pending":
          $panel_color = 'warning';
          $panel_message = 'This account is Pending';
          break;
      case "active":
          $panel_color = 'success';
          $panel_message = 'This account is Active';
          break;
      case "suspended":
          $panel_color = 'danger';
          $panel_message = 'this account is Suspended';
          break;
      default:
          echo "nothing";
  }
 ?>

 <div class="wrapper">
   <?php  include('layouts/side-navigation.php'); ?>
     <div class="main-panel">
         <!-- Navbar -->
         <?php include('layouts/header-navigation.php'); ?>
         <!-- End Navbar -->
         <div class="alert alert-<?php echo $panel_color;?>">
             <span>
                 <b> Account - </b> <?php echo $panel_message;?></span>
         </div>
         <div class="content">
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-md-12">
                         <div class="card ">
                             <div class="card-header ">
                                 <h4 class="card-title">Edit Worker</h4>
                                 <p class="card-category">Dashboard / Edit Worker</p>
                                 <a href="http://localhost/hlp-admin/views/worker_profile.php" class="btn btn-warning"><i class="nc-icon nc-bullet-list-67"></i> Worker Profile</a>
                                  <form class="" action="../router/router.php" method="post" enctype="multipart/form-data">
                                <div class="btn-group pull-right" data-toggle="buttons">
                                  <?php if ($data['application_status'] == 'pending' || $data['application_status'] == 'suspended') { ?>
                                    <label class="btn btn-success" onclick="active_status()">
                                      <input type="radio" name="application_status" value="active" id="option2"> <i class="nc-icon nc-check-2"></i> Active Account
                                    </label>
                                  <?php }else{ ?>
                                    <label class="btn btn-danger"  onclick="suspended_status()">
                                      <input type="radio" name="application_status" value="suspended" id="option1"> <i class="nc-icon nc-simple-remove"></i> Suspend Account
                                    </label>
                                  <?php } ?>
                              </div>
                             </div>
                             <div class="card-body ">
                                   <div class="form-group col-md-12">
                                     <label for="inputCity">Position</label>
                                     <input type="text" name="position" class="form-control" value="<?php echo $data['position']?>" id="inputCity" placeholder="Position">
                                   </div>
                                   <div class="form-row">
                                   <div class="form-group col-md-5">
                                     <label for="inputCity">Firstname</label>
                                     <input type="text" name="firstname" class="form-control" value="<?php echo $data['firstname']?>" id="inputCity" placeholder="Firstname">
                                   </div>
                                   <div class="form-group col-md-5">
                                     <label for="inputState">Lastname</label>
                                     <input type="text" name="lastname" class="form-control" value="<?php echo $data['lastname']?>" id="inputZip" placeholder="Lastname">
                                   </div>
                                   <div class="form-group col-md-2">
                                     <label for="inputZip">Middle Name</label>
                                     <input type="text" name="middlename" class="form-control" value="<?php echo $data['middlename']?>" id="inputZip" placeholder="Middlename">
                                   </div>
                                   </div>
                                   <div class="form-row">
                                   <div class="form-group col-md-2">
                                     <label for="inputCity">Age</label>
                                     <input type="text" name="age" class="form-control" value="<?php echo $data['age']?>" id="inputCity" placeholder="Age">
                                   </div>
                                   <div class="form-group col-md-5">
                                     <label for="inputState">Birthday</label>
                                     <input type="date" name="birthday" class="form-control" value="<?php echo date('Y-m-d',strtotime($data['birthday']))?>" id="inputZip" placeholder="Birthday">
                                   </div>
                                   <div class="form-group col-md-5">
                                     <label for="inputZip">Birthday Place</label>
                                     <input type="text" name="birth_place" class="form-control" value="<?php echo $data['birth_place']?>" id="inputZip" placeholder="Birth Place">
                                   </div>
                                   </div>
                                   <div class="form-row">
                                     <div class="form-group col-md-12">
                                       <label for="inputEmail4">Email</label>
                                       <input type="email" name="email" class="form-control" value="<?php echo $data['email']?>" id="inputEmail4" placeholder="Email">
                                     </div>
                                     </div>
                                     <div class="form-row">
                                     <div class="form-group col-md-4">
                                       <label for="inputCity">Gender</label>
                                       <select id="inputState" name="gender" class="form-control">
                                        <?php if($data['gender']=='male'){?>
                                          <option value="male">Male</option>
                                          <option value="female">Female</option>
                                        <?php }else{ ?>
                                          <option value="female">Female</option>
                                          <option value="male">Male</option>
                                        <?php } ?>
                                      </select>
                                     </div>
                                     <div class="form-group col-md-4">
                                       <label for="inputState">Mobile No.</label>
                                       <input type="text"  value="<?php echo $data['mobilenumber']?>" name="mobilenumber" class="form-control" id="inputCity" placeholder="Mobile No.">
                                     </div>
                                     <div class="form-group col-md-4">
                                       <label for="inputZip">Civil Status</label>
                                       <select id="inputState" name="civil_status" class="form-control">
                                        <?php if($data['civil_status']=='single'){?>
                                          <option value="single">single</option>
                                          <option value="married ">married</option>
                                        <?php }else{ ?>
                                          <option value="married ">married</option>
                                          <option value="single">single</option>
                                        <?php } ?>
                                      </select>
                                     </div>
                                     </div>
                                     <div class="form-group">
                                     <label for="inputAddress">Address</label>
                                     <input type="text" class="form-control" value="<?php echo $data['address']?>" name="address" id="inputAddress" placeholder="1234 Main St">
                                     </div>
                                     <div class="form-row">
                                     <div class="form-group col-md-4">
                                       <label for="inputCity">City</label>
                                       <input type="text" name="city" class="form-control" value="<?php echo $data['city']?>" id="inputCity">
                                     </div>
                                     <div class="form-group col-md-4">
                                       <label for="inputState">Province</label>
                                       <input type="text" value="<?php echo $data['province']?>" name="province" class="form-control" id="inputCity">
                                     </div>
                                     <div class="form-group col-md-4">
                                       <label for="inputZip">Zip</label>
                                       <input type="text" name="zip" value="<?php echo $data['zip']?>" class="form-control" id="inputZip">
                                     </div>
                                     </div>
                                     <hr>
                                     <div class="form-group">
                                       <div class="form-group col-md-4">
                                         <label for="inputZip">Tin</label>
                                         <input type="text" name="tin" value="<?php echo $data['tin']?>" class="form-control" id="inputZip">
                                       </div>
                                       <label for="inputZip">Nbi Clearance</label>
                                       <div class="custom-file">
                                         <input type="file" class="custom-file-input" name="nbiclearance" id="validatedCustomFile" >
                                         <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                         <div class="invalid-feedback">Example invalid custom file feedback</div>
                                       </div>
                                     </div>
                                     <div class="form-group">
                                       <label for="inputZip">Skill Certificate</label>
                                       <div class="custom-file">
                                         <input type="file" class="custom-file-input"  name="skillcertificate" id="validatedCustomFile" >
                                         <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                         <div class="invalid-feedback">Example invalid custom file feedback</div>
                                       </div>
                                     </div>
                                     <div class="form-group">
                                       <label for="inputZip">BP Clearance</label>
                                       <div class="custom-file">
                                         <input type="file" class="custom-file-input" name="bpclearance" id="validatedCustomFile" >
                                         <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                         <div class="invalid-feedback">Example invalid custom file feedback</div>
                                       </div>
                                     </div>
                                     <div class="row">
                                     <div class="col-md-4">
                                       <h2>Nbi Clearance</h2>
                                       <div class="thumbnail">
                                         <a href="../assets/images/<?php echo $data['nbi_clearance']?>">
                                           <img src="../assets/images/<?php echo $data['nbi_clearance']?>" alt="Lights" style="width:100%;">
                                         </a>
                                       </div>
                                     </div>
                                     <div class="col-md-4">
                                      <h2>Skill Certificate</h2>
                                       <div class="thumbnail">
                                         <a href="../assets/images/<?php echo $data['skill_certificate']?>">
                                           <img src="../assets/images/<?php echo $data['skill_certificate']?>" alt="Lights" style="width:100%;">
                                         </a>
                                       </div>
                                     </div>
                                     <div class="col-md-4">
                                        <h2>BP Clearance</h2>
                                       <div class="thumbnail">
                                         <a href="../assets/images/<?php echo $data['b_p_clearance']?>">
                                           <img src="../assets/images/<?php echo $data['b_p_clearance']?>" alt="Lights" style="width:100%;">
                                         </a>
                                       </div>
                                     </div>
                                    </div>
                                       <input type="text" name="worker_id" value="<?php echo $data['worker_id']?>" style="display:none;">
                                     <button type="submit" class="btn btn-primary" name="update_worker" value="update_worker">Update</button>
                                 </form>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>

<?php  include('layouts/footer.php'); ?>
