<div class="sidebar" data-color="blue" data-image="../assets/img/sidebar-5.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                HLP - ADMIN
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="../views/dashboard.php">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="../views/user_profile.php">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>User Profile</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="../views/add_worker.php">
                    <i class="nc-icon nc-badge"></i>
                    <p>Add Worker</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="../views/worker_profile.php">
                    <i class="nc-icon nc-notes"></i>
                    <p>Worker Profile</p>
                </a>
            </li>
            <li class="nav-item active active-pro">
                <a class="nav-link active" href="../views/logout.php">
                    <i class="nc-icon nc-button-power"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </div>
</div>
