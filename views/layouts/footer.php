</body>
<script>
function active_status(){
  swal("Actived!", "Account is now Active!", "success");
}

function suspended_status(){
  swal("Suspend!", "Account is now suspended!", "success");
}
</script>
<!-- router -->
<script src="../router/router.js"></script>
<!-- user controller -->
<script src="../controller/userController.js"></script>
<!-- SweetAlert -->
<script src="../modules/dist/sweetalert.js"></script>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Chartist Plugin  -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="../assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>

<!-- Page level plugin JavaScript-->
<script src="../modules/datatables/jquery.dataTables.js"></script>
<script src="../modules/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for this page-->
<script src="../modules/datatables/sb-admin-datatables.min.js"></script>

</html>
