	<?php include('layouts/header.php') ?>
	<div class="gtco-loader"></div>

	<div id="page">

		<?php include('layouts/navigation.php') ?>

		<header id="gtco-header" class="gtco-cover" role="banner">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-12 col-md-offset-0 text-left">
						<div class="display-t">
							<div class="display-tc">
								<div class="row">
									<div class="col-md-6 col-md-offset-3 text-center header-img animate-box">
										<h1>Register</h1>
										<form action="../../router/router.php" method="post">
										  <div class="form-group">
										    <label for="email">Email address:</label>
										    <input type="email" name="email" class="form-control" id="email" required>
										  </div>
										  <div class="form-group">
										    <label for="email">First Name:</label>
										    <input type="text" name="firstname" class="form-control" id="email" required>
										  </div>
										  <div class="form-group">
										    <label for="email">Last Name:</label>
										    <input type="text" name="lastname" class="form-control" id="email" required>
										  </div>
										  <div class="form-group">
										    <label for="email">Address:</label>
										    <input type="text" name="address" class="form-control" id="email" required>
										  </div>
										  <div class="form-group">
										    <label for="email">Mobile Number:</label>
										    <input type="text" name="mobilenumber" class="form-control" id="email" required>
										  </div>
										  <div class="form-group">
										    <label for="pwd">Password:</label>
										    <input type="password" name="password" class="form-control" id="pwd" required>
										  </div>
										  <button type="submit" value="register" name="register" class="btn btn-default">Submit</button>
										</form>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- END #gtco-header -->
<?php include('layouts/footer.php') ?>
